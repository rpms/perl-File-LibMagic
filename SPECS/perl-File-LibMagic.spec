# Filter the Perl extension module
%{?perl_default_filter}

%define module	File-LibMagic

Name:		perl-%{module}
Version:	1.16
Release:	2%{?dist}
Summary:	Perl wrapper/interface for libmagic
License:	GPL+ or Artistic
Source:		http://search.cpan.org/CPAN/authors/id/D/DR/DROLSKY/%{module}-%{version}.tar.gz
URL:		http://search.cpan.org/dist/%{module}/
BuildRequires:	%{_includedir}/magic.h
BuildRequires:	coreutils
BuildRequires:	findutils
BuildRequires:	make
BuildRequires:	perl-devel
BuildRequires:	perl-generators
BuildRequires:	perl-interpreter
BuildRequires:	perl(Config::AutoConf)
BuildRequires:	sed
# ExtUtils::CBuilder needed for Config::AutoConf to handle C language
BuildRequires:	perl(ExtUtils::CBuilder)
BuildRequires:	perl(ExtUtils::MakeMaker)
BuildRequires:	perl(Getopt::Long)
BuildRequires:	perl(lib)
BuildRequires:	perl(strict)
BuildRequires:	perl(warnings)
# Run-time:
BuildRequires:	perl(base)
BuildRequires:	perl(Carp)
BuildRequires:	perl(Exporter)
BuildRequires:	perl(Scalar::Util)
BuildRequires:	perl(XSLoader)
# Tests:
BuildRequires:	perl(Cwd)
BuildRequires:	perl(File::Spec)
BuildRequires:	perl(File::Temp)
BuildRequires:	perl(IO::Handle)
BuildRequires:	perl(IPC::Open3)
# Pod::Coverage::TrustPod not used
# Pod::Wordlist not used
# Test::Code::TidyAll 0.24 not used
# Test::CPAN::Changes not used
# Test::EOL not used
BuildRequires:	perl(Test::Fatal)
BuildRequires:	perl(Test::More)
# Test::Mojibake not used
# Test::NoTabs not used
# Test::Pod 1.41 not used
# Test::Pod::Coverage 1.08 not used
# Test::Spelling 0.12 not used
# Test::Synopsis not used
# Test::Version not used
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
The File::LibMagic is a simple perl interface to libmagic from the
file (4.x or 5.x) package.

%prep
%setup -q -n %{module}-%{version}
# Remove bundled modules
rm -r inc
sed -i -e '/^inc\//d' MANIFEST

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -delete
# Remove files installed by an accident, CPAN RT#107081
find %{buildroot} -type f \( -name LibMagic.xs -or -name typemap \) -delete
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes CONTRIBUTING.md README.md
%{perl_vendorarch}/File/
%{perl_vendorarch}/auto/File/
%{_mandir}/man3/File::LibMagic.3*

%changelog
* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.16-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Oct 24 2017 Paul Howarth <paul@city-fan.org> - 1.16-1
- 1.16 bump

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.15-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.15-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jun 05 2017 Jitka Plesnikova <jplesnik@redhat.com> - 1.15-5
- Perl 5.26 rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.15-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sun May 15 2016 Jitka Plesnikova <jplesnik@redhat.com> - 1.15-3
- Perl 5.24 rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.15-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Nov 09 2015 Jitka Plesnikova <jplesnik@redhat.com> - 1.15-1
- 1.15 bump

* Mon Sep 14 2015 Petr Pisar <ppisar@redhat.com> - 1.13-1
- 1.13 bump

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.00-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 03 2015 Jitka Plesnikova <jplesnik@redhat.com> - 1.00-5
- Perl 5.22 rebuild

* Wed Aug 27 2014 Jitka Plesnikova <jplesnik@redhat.com> - 1.00-4
- Perl 5.20 rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.00-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.00-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Sep 09 2013 Jitka Plesnikova <jplesnik@redhat.com> - 1.00-1
- 1.00 bump
- Update source link
- Specify all dependencies

* Tue Aug 27 2013 Josh Kayse <jokajak@gmail.com> 0.99-1
- update to 0.99

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.96-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Jul 17 2013 Petr Pisar <ppisar@redhat.com> - 0.96-10
- Perl 5.18 rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.96-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.96-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jun 11 2012 Petr Pisar <ppisar@redhat.com> - 0.96-7
- Perl 5.16 rebuild

* Mon Apr 23 2012 Paul Howarth <paul@city-fan.org> - 0.96-6
- Update test suite to work with file 5.10 (CPAN RT#75457)
- Don't need to link against libz (CPAN RT#56479)

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.96-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Nov 23 2011 Ville Skyttä <ville.skytta@iki.fi> - 0.96-4
- Own vendor_perl/File dirs.
- Include Changes in docs.

* Wed Jun 15 2011 Marcela Mašláňová <mmaslano@redhat.com> - 0.96-3
- Perl mass rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.96-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Dec 04 2010 Robert Scheck <robert@fedoraproject.org> 0.96-1
- Upgrade to 0.96 and some spec file cleanup
- Replaced Test::Base by Test::More (thanks to Andreas Koenig)

* Sat May 01 2010 Marcela Maslanova <mmaslano@redhat.com> - 0.91-4
- Mass rebuild with perl-5.12.0

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 0.91-3
- rebuild against perl 5.10.1

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.91-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Mar 22 2009 Robert Scheck <robert@fedoraproject.org> 0.91-1
- Upgrade to 0.91 and some spec file cleanup

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.88-1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jul 28 2008 Josh Kayse <josh.kayse@gtri.gatech.edu> 0.88-0.f10
- update to 0.88

* Sat Mar 01 2008 Josh Kayse <josh.kayse@gtri.gatech.edu> 0.85-3
- add perl Require
- specify specific directories in files

* Fri Feb 29 2008 Josh Kayse <josh.kayse@gtri.gatech.edu> 0.85-2
- added patch to fix test cases

* Thu Feb 28 2008 Josh Kayse <josh.kayse@gtri.gatech.edu> 0.85-1
- update to 0.85

* Mon Oct 10 2005 Nicolas Lécureuil <neoclust@mandriva.org> 0.82-5mdk
- Fix previous mistake

* Fri Sep 30 2005 Nicolas Lécureuil <neoclust@mandriva.org> 0.82-4mdk
 - buildrequires fix

* Thu Sep 29 2005 Nicolas Lécureuil <neoclust@mandriva.org> 0.82-3mdk
- fix url
- fix buildrequires

* Sun Jun 19 2005 Olivier Thauvin <nanardon@mandriva.org> 0.82-2mdk
- patch0: add search ldflags
- BuildRequires libmagic-devel

* Wed Jun 15 2005 Olivier Thauvin <nanardon@mandriva.org> 0.82-1mdk
- First mandriva spec
